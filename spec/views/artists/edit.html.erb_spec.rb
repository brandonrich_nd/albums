require 'rails_helper'

RSpec.describe "artists/edit", type: :view do
  before(:each) do
    @artist = assign(:artist, Artist.create!(
      :name => "MyString",
      :bio => "MyText",
      :photo_url => "MyString",
      :year_formed => "MyString"
    ))
  end

  it "renders the edit artist form" do
    render

    assert_select "form[action=?][method=?]", artist_path(@artist), "post" do

      assert_select "input#artist_name[name=?]", "artist[name]"

      assert_select "textarea#artist_bio[name=?]", "artist[bio]"

      assert_select "input#artist_photo_url[name=?]", "artist[photo_url]"

      assert_select "input#artist_year_formed[name=?]", "artist[year_formed]"
    end
  end
end
