require 'rails_helper'

RSpec.describe "artists/index", type: :view do
  before(:each) do
    assign(:artists, [
      Artist.create!(
        :name => "Name",
        :bio => "MyText",
        :photo_url => "Photo Url",
        :year_formed => "Year Formed"
      ),
      Artist.create!(
        :name => "Name",
        :bio => "MyText",
        :photo_url => "Photo Url",
        :year_formed => "Year Formed"
      )
    ])
  end

  it "renders a list of artists" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Photo Url".to_s, :count => 2
    assert_select "tr>td", :text => "Year Formed".to_s, :count => 2
  end
end
